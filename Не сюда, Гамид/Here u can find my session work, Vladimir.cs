﻿using System;

namespace Task
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int num = rand.Next(40, 50);

            while (num != 1)
            {
                if (num % 2 == 0)
                {
                    num = num / 2;
                }
                else
                {
                    num = (num * 3 + 1) / 2;
                }

                Console.WriteLine(num);
            }

            Console.WriteLine($"Число равное {num}");
        }
    }
}