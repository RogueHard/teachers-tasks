﻿using System;

public class Program
{
  public static void Main(string[] args)
  {
    int y = 0;
    int x = Convert.ToInt32(Console.ReadLine());

    if (x > 0)
    {
      y = 2*x-10;
    }else if (x == 0) {
      y = 0;
    }else{
      y = 2*Math.Abs(x)-1;
    }
    Console.WriteLine(y);
  }
}