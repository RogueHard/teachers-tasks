﻿using System;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                int firstNum = Convert.ToInt32(Console.ReadLine());
                int secondNum = Convert.ToInt32(Console.ReadLine());
                if (firstNum % secondNum == 0)
                {
                    Console.WriteLine("Без остатка");
                    Console.WriteLine(firstNum / secondNum);
                }
                else if (firstNum % secondNum != 0)
                {
                    Console.WriteLine("С остатком");
                    Console.WriteLine(firstNum / secondNum + ". Остаток: " + firstNum % secondNum);
                }
            }
        }
    }
}
