﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
                Console.WriteLine("Выберите вариант перевода (введите номер):");
                Console.WriteLine("1. Байты.");
                Console.WriteLine("2. Килобайты.");
                int a = Convert.ToInt32(Console.ReadLine());
            if (a > 2)
                {
                    Console.WriteLine("Данная программа поддерживает только переводы в байты и килобайты. В следующий раз введите корректное число");
                }else if (a == 1 == true)
                {
                    Console.WriteLine("Введите число, которое Вы хотите перевести в байты");
                int b = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine($"{b} = {b * 1024} байт.");
            }
                else
                {
                    Console.WriteLine("Введите число, которое Вы хотите перевести в килобайты");
                int c = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine($"{c} = {c / 1024} килобайт.");
            }
        }
    }
}
