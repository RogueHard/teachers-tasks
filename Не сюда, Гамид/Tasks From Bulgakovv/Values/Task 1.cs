﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            int value1 = 3;
            int value2 = 4;
            int value3 = 0;
            value3 = value1;
            value1 = value2;
            value2 = value3;
            value3 = 0;
            Console.WriteLine($"value1 = {value1}, value2 = {value2}");
        }
    }
}