﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            int milkPrice = 56;
            int matchesPrice = 2;
            string question1 = "Сколько пачек молока Вы купили?";
            string question2 = "Сколько коробок спичек Вы купили?";
            Console.WriteLine(question1);
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(question2);
            int b = Convert.ToInt32(Console.ReadLine());
            int c = a * (milkPrice);
            int d = b * (matchesPrice);
            Console.WriteLine($"На молоко Вы потратили {c}, а на спички {d}. В общей сумме на покупки у Вас ушло {c + d}.");
        }
    }
}