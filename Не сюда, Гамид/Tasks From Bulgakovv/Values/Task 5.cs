﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                int q = Convert.ToInt16(Console.ReadLine());
                int w = Convert.ToInt16(Console.ReadLine());
                if (q % w == 0)
                {
                    Console.WriteLine($"Деление выполнено без остатка. Ответ: {q / w}");
                }
                else
                {
                    Console.WriteLine($"Деление выполнено с остатком. Ответ: {q / w}, остаток: {q % w}");
                }
            }
        }
    }
}